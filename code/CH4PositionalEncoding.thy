(* SECTION 4.4: POSITIONAL ENCODING OF THE PROTOCOL *)

theory CH4PositionalEncoding

imports
  Main
  CH4RegisterMachine
  CH2PositionalNotation CH2DigitComp
begin

(* Definitions implemented by Abhik Pal, Marco David *)

(* [D] 4.14 *)
definition B :: "nat \<Rightarrow> nat" where
  "(B c) = 2^(Suc c)"

(* [D] 4.18 *)
(* Is this useful in light of geometric series? *)
definition E :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
  "(E q b) = (\<Sum>t\<le>q. (b^t))"
lemma E_simps[simp]:
  \<open>E 0 b = 1\<close> 
  \<open>E (Suc q) b = b^(Suc q) + E q b\<close>
  using E_def by auto

(* [D] 4.21 *)
definition F :: "nat \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> nat" where
  "(F q c b) = (\<Sum>t\<le>q. (2^c) * (b^t))"

(* [D] 4.16 *)
definition D :: "nat \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> int" where
  "(D q c b) = int (F q c b) - int (E q b)"

end