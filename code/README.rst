Hilbert meets Isabelle
======================

Das "Hilbert meets Isabelle" Projekt formalisiert Yuri Matiyasevichs
Beweis des zehnten Hilbertschen Problems mit dem interaktiven
Beweisassistenten Isabelle.

Dieses Reposiotry umfasst all den Code, der für das Jugend Forscht
2018 Projekt von Marco David, Abhik Pal und Benedikt Stock, Jacobs
University Bremen geschrieben wurde, sowie wenige Unterstützungsleistungen,
auf welchen die Implementierung aufbaut. Das Projekt wird betreut von
Prof. Dr. Dierk Schleicher.

Inhaltsverzeichnis (Stand 26. April 2018)
------------------

**Funktionen und Definitionen**

- *CH2PositionalNotation.thy* -- Implementierung des Stellenwertsystems aus Kapitel 2 von Matiyasevichs Beweis

- *CH2DigitComp.thy* -- Implementierung der Maskierung und binären Orthogonalität um Zahlen Ziffer für Ziffer zu vergleichen.

- *CH4RegisterMachine.thy* -- Implementierung der Sturktur und Grundeigenschaften von Registermaschinen aus Kapitel 4 von Matiyasevichs Beweis

- *CH4PositionalEncoding.thy* -- Konvertierung des Protkolls in ein Stellenwertsystem (und zurück)

**Lemmata und Beweise**

- *CH2Diophantine.thy* -- Teil-Implementierung von Lemmata zu Diophantischen Representationen aus Matiyasevichs zweiten Kapitel

- *CH3ExponentialDiophantine.thy* -- Vollständige Formalisierung von Kapitel 3 von Matiyasevichs Beweis (inzwischen fertiggestellt, aber noch nicht komplett veröffentlicht)

- *CH4Simulation.thy* -- Lemmata aus dem vierten Kapitel, diophantische Simulierung einer Registermaschine, größtenteils noch zu beweisen

- *CH4ListableDiophantine.thy* -- Abschluss des vierten Kapitels, Fazit: "All listable sets are diophantine."

Eigenanteil
-----------

Jeglicher Code, der als Unterstützungsleistung gilt, also nicht von Abhik,
Benedikt oder Marco geschrieben wurde, ist in diesem Repository klar durch
Kommentare wie ``` (* comment *) ``` gekennzeichnet.

Insbesondere sind die Folgenden Mitwirkenden zu nennen:

- Yufei Liu, der beachtliche Teile zu *CH3ExponentialDiophantine.thy* und *CH2Diophantine.thy* beigetragen hat

- Yiping Scott Deng, der Teile zu den Dateien von Kapitel 4, sowie große Teile zu dessen konzeptuelle Ausarbeitung beigetragen hat

- Deepak Aryal, Prabhat Devkota und Simon Dubischar, die Teile zu *CH3ExponentialDiophantine.thy* beigetragen haben

Damit hat Benedikt Stock ca. die Hälfte von *ExponentialDiophantine.thy*, der
vollständigen Implementierung von Kapitel 3, geschrieben.
Abhik Pal und Marco David haben vom aktuellen Forschritt in den restlichen
Dateien, der halb vervollständigten Implementierung von Kapitel 4 und 2,
bis auf einige Funktionen und Lemmata, fast alles implementiert.

Lizenz
------

HILBERT MEETS ISABELLE

Formalisierung von Yuri Matiyasevichs Beweis des zehnten Hilbertschen
Problems

Copyright (C) 2017-2018
Abhik Pal, Marco David, Benedikt Stock, Deepak Aryal, Yiping Deng, Yufei Liu

Dieses Programm ist freie Software. Sie können es unter den
Bedingungen der GNU General Public License, wie von der Free Software
Foundation veröffentlicht, weitergeben und/oder modifizieren, entweder
gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren
Version.

Die Veröffentlichung dieses Programms erfolgt in der Hoffnung, daß es
Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE, sogar ohne
die implizite Garantie der MARKTREIFE oder der VERWENDBARKEIT FÜR
EINEN BESTIMMTEN ZWECK. Details finden Sie in der GNU General Public
License.

Sie sollten ein Exemplar der GNU General Public License zusammen mit
diesem Programm erhalten haben. Falls nicht, siehe
<http://www.gnu.org/licenses/>.

Nur die englische Version der Lizenz ist rechtskräftig.
Nichtsdestotrotz ist hier
<http://www.gnu.de/documents/gpl-3.0.de.html> eine inoffizielle
deutsche Überstzung des Lizenztextes zu finden.
