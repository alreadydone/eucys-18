theory CH4ListableDiophantine

(* SECTION 4.6: ALL LISTABLE SETS ARE DIOPHANTINE *)

imports 
    Main
    CH4PositionalEncoding
    CH2DigitComp
begin

subsection {* Geometric Series *}
(*
 * In fact, for (4.16), (4.18), (4.21) we can simply sum up the geomtric
 * progressions to so these conditions can be re-written as:
 * All work done by Abhik Pal.
 *)

lemma finite_geometric[simp]:
  fixes a :: int
  fixes r :: nat
  fixes n :: nat
  shows "(int 1 - int r) * (\<Sum>k<(Suc n). a * int (r ^k)) = 
          a * (int 1 - int (r ^ (Suc n)))"
proof(induct n)
  case 0
  show ?case by simp
next
  case (Suc n)
  have f1: "(1 - int r) * (\<Sum>n<Suc n. a * int (r ^ n)) = a * (1 - int (r ^ Suc n))"
    using Suc.hyps transfer_int_nat_numerals(2) by algebra
  have f2: "\<And>i ia. (i::int) * ia - ia = (i - 1) * ia"
    by (simp add: int_distrib(3))
  have f3: "\<And>i ia. (i::int) - ia - i = - ia"
    by auto
  have f4: "(1 - int r) * (\<Sum>n<Suc (Suc n). a * int (r ^ n)) =
            a * (1 - int r ^ Suc n + (1 - int r) * int r ^ Suc n)"
    using f1 by (simp add: semiring_normalization_rules(34))
  have f5: "\<And>i ia. - (i::int) * ia = - (i * ia)"
    by auto
  have f6: "\<And>i ia. (i::int) + ia = i - - ia"
    by simp
  have f7: "\<And>i ia ib. (i::int) - ia - - ib = i - - (ib - ia)"
    by presburger
  have "a * (1 - - (- (int r * int r ^ Suc n))) = a * (1 - int r ^ Suc (Suc n))"
    by simp
  then have "(1 - int r) * (\<Sum>n<Suc (Suc n). a * int (r ^ n)) =
              a * (1 - int r ^ Suc (Suc n))"
    using f7 f6 f5 f4 f3 f2 by presburger
  then show ?case
    by auto
qed

lemma lm04_32_geomtric_sum_0418:
  fixes q c :: nat
  defines "b == (B c)"
  defines "e == int (E q b)"
  shows "(int 1 - int b) * e = int 1 - int (b ^ (Suc q))"
proof-
  from e_def have f0:"e = (\<Sum>k<Suc q. 1 * int (b ^ k))" by simp
  from finite_geometric f0 show ?thesis by presburger
qed
  
lemma lm04_33_geometric_sum_0421:
  fixes c q :: nat
  defines "b == (B c)"
  defines "f == int (F q c b)"
  defines "a == int (2 ^ c)"
  shows "(1 - int b) * f = (2 ^ c) * (int 1 - int (b ^ (Suc q)))"
proof-
  from f_def a_def have f0:"f = (SUM k<(Suc q). a * int (b ^ k))" by simp
  from finite_geometric f0 a_def show ?thesis by auto
qed

lemma lm04_31_geometric_sum_0416:
  fixes q c :: nat
  defines "b == (B c)"
  defines "d == (D q c b)"
  defines "e == int (E q b)"
  defines "f == int (F q c b)"
  defines "a == (int 2^c - int 1)"
  shows "(int 1 - int b) * d =  a * (int 1 - int (b ^ (Suc q)))"
proof-
  from D.simps d_def e_def f_def
    have f0:"d = f - e"
    by blast
  
  from lm04_33_geometric_sum_0421 b_def f_def
    have f1:"(1 - int b) * f = (2 ^ c) * (int 1 - int (b ^ (Suc q)))"
    by blast
  
  from lm04_32_geomtric_sum_0418 b_def e_def 
    have f2:"(int 1 - int b) * e = int 1 - int (b ^ (Suc q))"
    by blast

  from f2 f1
    have f3:"(1 - int b) * f - (int 1 - int b) * e =
      (2 ^ c) * (int 1 - int (b ^ (Suc q))) - (int 1 - int (b ^ (Suc q)))"
    by simp
  
  from a_def of_nat_1 semiring_normalization_rules(2) transfer_int_nat_numerals(3)
    have f4:"(2 ^ c) * (int 1 - int (b ^ (Suc q))) - (int 1 - int (b ^ (Suc q))) =
            a * (int 1 - int (b ^ (Suc q)))"
    by smt

  from f0 f1 f2 f4 int_distrib(3) mult.commute transfer_int_nat_numerals(2)
    show ?thesis
    by metis
qed

subsection {* All Listable sets are Diophantine *}

(* Final Chapter 4 Theorem, Marco David & Scott Deng *)
theorem listable_implies_diophantine:
  (* Given a valid register machine that accepts a set... *)
  fixes ic :: configuration
    and p :: program
    and a :: nat
    and q :: nat
    and c :: nat
  assumes "is_valid ic p a"
  assumes "terminates ic p q"

  defines "s0 \<equiv> fst ic"
      and "tape0 \<equiv> snd ic"
  defines "n \<equiv> length tape0"
      and "m \<equiv> length p"

  (* all configurations *)
  defines "cl \<equiv> run ic p q"
  defines "rlt \<equiv> \<lambda>t l. (snd (cl!t))!l"
      and "zlt \<equiv> \<lambda>t l. (zero (snd (cl!t)) l)"
      and "skt \<equiv> \<lambda>t k. (state_column (fst (cl!t)) m)!k"

  (* to be exported to a function that verifies this *)
  assumes "\<And>l t :: nat. (l < n & t \<le> q) \<longrightarrow> (2^c > rlt t l & 2^c > zlt t l)"
  assumes "\<And>k t :: nat. (k < m & t \<le> q) \<longrightarrow> (2^c > skt t k)"
  assumes "c > 0" (* needs to be proven from the above *)

  (* ... obtain certain numbers b, c, d, e, f and r, s, z ... *)
  defines "b \<equiv> B c"
  defines "d \<equiv> D q c b"
      and "e \<equiv> E q b"
      and "f \<equiv> F q c b"

  (* the below are functions of l and k, respectively *)
  defines "r \<equiv> \<lambda>l. RL q ((register_rows cl)!l) b"
      and "z \<equiv> \<lambda>l. ZL q ((zero_rows cl)!l) b"
      and "s \<equiv> \<lambda>l. SK q ((state_rows cl p)!l) b"

  (* ... there exists (find a) diophantine representation of the register machine. *)
  (*     This representation need not be unique.                                       *)
  shows "  (b = 2^(Suc c))
         \<and> (b^q = s m)
         \<and> (a < 2^c)
         \<and> (\<forall>l :: nat. (l < n) \<longrightarrow> nat d msk (r l))
         \<and> (\<forall>l :: nat. (l < n) \<longrightarrow> e msk (z l))
         \<and> (\<forall>k :: nat. (k < m) \<longrightarrow> 2^c * (z l) = (r l + nat d) && f)
         \<and> ((b-1) * nat d = (2^c - 1) * (b^(q + 1) - 1))
         \<and> ((b-1) * e = (b^(q + 1) - 1))
         \<and> ((b-1) * f = 2^c * (b^(q+1) - 1))"
proof -
  from lm04_33_geometric_sum_0421 have "(1 - int b) * int f = (2 ^ c) * (int 1 - int (b ^ (Suc q)))"
    using b_def f_def by blast
  moreover from `c > 0` have "b > 1" using b_def
    by (metis B.elims One_nat_def add_Suc_right arith_special(3) lessI of_nat_0 of_nat_1 one_less_power semiring_1_class.of_nat_simps(2) zero_less_Suc)
  moreover from this have "b - 1 > 0" by simp
  moreover from `b > 1` have "b^(q+1) - 1 > 0"
    by (meson add_gr_0 one_less_power zero_less_diff zero_less_one)
  moreover from this `b - 1 > 0` have "-1 * (1 - int b) * int f = -1 * (2 ^ c) * (int 1 - int (b ^ (Suc q)))"
    using calculation(1) by linarith
  ultimately have inverted: "nat ((int b - 1) * int f) = nat (2^c * (int b^(q+1) - 1))"
    by (metis (no_types, hide_lams) Suc_eq_plus1 minus_diff_eq mult_minus_left mult_minus_right of_nat_1 of_nat_power)
  from f_def have "f > 0"
    by fastforce
  then have "nat (int f) = f" by simp
  from this inverted have "(b - 1) * f = 2^c * (b^(q+1) - 1)" sorry

  thus ?thesis sorry
qed

end